package th.ac.tu.siit.convertcalculator;

import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SettingActivity extends Activity implements OnClickListener {
	
	float exchangeRate;
	float IntRate;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting);
		
		Button b1 = (Button)findViewById(R.id.btnFinish);
		b1.setOnClickListener(this);
		
		
		Intent i = this.getIntent();
		exchangeRate = i.getFloatExtra("exchangeRate", 32.0f);
		
		Intent j = this.getIntent();
		IntRate = j.getFloatExtra("IntRate", 10.0f);
		
		EditText etRate = (EditText)findViewById(R.id.etRate);
		etRate.setText(String.format(Locale.getDefault(), "%.2f", exchangeRate));
		
		EditText inRate = (EditText)findViewById(R.id.inRate);
		inRate.setText(String.format(Locale.getDefault(), "%.2f", IntRate));
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.setting, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		EditText etRate = (EditText)findViewById(R.id.etRate);
		EditText inRate = (EditText)findViewById(R.id.inRate);
		try {
			
			Intent data = new Intent();// Add the return value to the intent
			
			float r = Float.parseFloat(etRate.getText().toString());// we create an intent to wrap to the value
			data.putExtra("exchangeRate", r);// Set the intent as the result when this activity ends
			
			float s = Float.parseFloat(inRate.getText().toString());// we create an intent to wrap to the value
			data.putExtra("IntRate", s);// Set the intent as the result when this activity ends
			
			
			this.setResult(RESULT_OK, data);// End this activity
			this.finish();
			
			
		} catch(NumberFormatException e) {
			Toast t = Toast.makeText(getApplicationContext(), 
					"Invalid exchange rate", Toast.LENGTH_SHORT);
			t.show();
			etRate.setText("");
			etRate.requestFocus();
		} catch(NullPointerException e) {
			Toast t = Toast.makeText(getApplicationContext(), 
					"Invalid exchange rate", Toast.LENGTH_SHORT);
			t.show();
			etRate.setText("");
			etRate.requestFocus();
		}
	}
	
}